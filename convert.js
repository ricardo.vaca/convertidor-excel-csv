function xlsxToCSV() {
  var file = document.getElementById("file-selector").files[0];

  file.arrayBuffer().then((res) => {
    let data = new Uint8Array(res);
    let workbook = XLSX.read(data, { type: "array" });
    let first_sheet_name = workbook.SheetNames[0];
    let worksheet = workbook.Sheets[first_sheet_name];
    let csv = XLSX.utils.sheet_to_csv(worksheet);
    // download csv file
    let csvData = new Blob([csv], { type: "text/csv" });
    let csvURL = window.URL.createObjectURL(csvData);
    let tempLink = document.createElement("a");
    tempLink.href = csvURL;
    tempLink.setAttribute("download", "data.csv");
    tempLink.click(); 
  });
}
